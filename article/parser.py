import requests
from bs4 import BeautifulSoup
from bs4.dammit import EncodingDetector

from article.models import Author, Article

PARSE_URL = 'https://rb.ru'


def parser(url):
    response = requests.get(url)
    http_encoding = response.encoding if 'charset' in response.headers.get('content-type', '').lower() else None
    html_encoding = EncodingDetector.find_declared_encoding(response.content, is_html=True)
    encoding = html_encoding or http_encoding
    soup = BeautifulSoup(response.content, from_encoding=encoding, features='html.parser')
    return soup


def parser_all_links():
    url = f'{PARSE_URL}/news/'
    soup = parser(url)
    quotes = soup.find_all('a', href=True)
    links = set()

    for item in quotes:
        if '/news/' in item['href']:
            links.add(item['href'])

    return links


def parser_data_article():
    links = list(parser_all_links())
    for i in range(30):
        url = f'{PARSE_URL}{links[i]}'
        soup = parser(url)
        name = soup.find('div', {'id': 'kpi'}).text.split()
        content_begin = soup.find_all('div', {'class': 'article__introduction'})
        content_end = soup.find('div', {'class': 'article__content-block abv'}).find_all('p')
        title = soup.find('div', {'style': 'display: none;'}).text
        article = Article.objects.create(
            title=title,
            content=content_begin+content_end
        )
        author = Author.objects.create(
            author_first_name=name[0],
            author_last_name=name[1]
        )
        article.author.add()
        return article
