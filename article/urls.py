from django.urls import path, include
from . import views


urlpatterns = [
    path('signup/', views.SignUp.as_view(), name='signup'),
    path('auth/', include('django.contrib.auth.urls')),
    path('new/', views.new_article, name='new_article'),
    path('authors/', views.author_all, name='author_all'),
    path('article/', views.article_all, name='article'),
    path('', views.catalog, name='catalog'),
]
