import factory
from django.db import models


class Author(models.Model):
    author_first_name = models.CharField(
        verbose_name='Имя',
        max_length=50,
        blank=False
    )
    author_last_name = models.CharField(
        verbose_name='Фамилия',
        max_length=50,
        blank=False
    )

    class Meta:
        ordering = ('-author_first_name',)


class Article(models.Model):
    title = models.CharField(
        verbose_name='Заголовок статьи',
        max_length=50
    )
    content = models.TextField(
        verbose_name='Содержание статьи',
    )
    author = models.ManyToManyField(Author, blank=True)

    def __str__(self):
        return f'{self.author} {self.content}'


class AuthorFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Author

    author_first_name = factory.Faker('first_name')
    author_last_name = factory.Faker('last_name')
