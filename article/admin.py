from django.contrib import admin
from .models import Article, Author


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'content')
    search_fields = ('author',)
    empty_value_display = '-пусто-'


@admin.register(Author)
class AuthorAdmin(admin.ModelAdmin):
    pass
