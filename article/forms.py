from django import forms
from django.utils.translation import gettext_lazy as _
from .models import Article, Author
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model

User = get_user_model()


class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ('title', 'content', 'author',)
        labels = {
            'author': _('Автор'),
            'title': _('Заголовок'),
            'content': _('Содержание'),
        }
        help_texts = {
            'author': _('Выберите автора из списка'),
            'title': _('Напишите заголовок новости'),
            'content': _('Поделитесь информацией со всем миром'),
        }


class AuthorForm(forms.ModelForm):
    class Meta:
        model = Author
        fields = ('author_first_name', 'author_last_name',)
        labels = {
            'author_first_name': _('Имя автора'),
            'author_last_name': _('Фамилия автора'),
        }


class CreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('first_name', 'last_name', 'username', 'email')
