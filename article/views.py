from django.contrib.auth import get_user_model
from django.shortcuts import redirect, render, get_object_or_404
from article.forms import ArticleForm, CreationForm
from article.models import Article, Author
from django.urls import reverse_lazy
from django.views.generic import CreateView

User = get_user_model()


def catalog(request):
    return render(request, 'index.html')


def author_all(request):
    author_list = Author.objects.all()
    return render(
        request,
        'author.html',
        {'author_list': author_list})


def article_all(request):
    article_list = Article.objects.all()
    return render(
        request,
        'article.html',
        {'article_list': article_list})


def new_article(request):
    form = ArticleForm(request.POST or None, files=request.FILES or None)
    if form.is_valid():
        article = form.save(commit=False)
        article.save()
        article.author.add()
        return redirect('catalog')
    return render(request, 'new_article.html', {'form': form})


def profile(request, author_id):
    author = Author.objects.get(id=author_id)
    article_list = author.article_set.all()
    return render(
        request,
        'profile.html',
        {
            'article_list': article_list,
            'author': author,
        }
    )


def article_view(request, username, article_id):
    form = ArticleForm(request.POST or None, files=request.FILES or None)
    article = get_object_or_404(Article, pk=article_id)
    author = get_object_or_404(Author, author_first_name=username)
    article_list = article.author.article_set.all()
    return render(
        request,
        'article.html',
        {'author': author,
         'article': article,
         'articles': article_list,
         'form': form}
    )


class SignUp(CreateView):
    form_class = CreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'
