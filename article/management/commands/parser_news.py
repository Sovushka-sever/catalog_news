from django.core.management.base import BaseCommand
from article.parser import parser_data_article


class Command(BaseCommand):

    help = 'Parser news'

    def add_arguments(self, parser):
        parser.add_argument(
            '--parser',
        )

    def handle(self, *args, **options):
        for _ in range(options['parser']):
            parser_data_article()
