from django.core.management.base import BaseCommand
from article.models import AuthorFactory


class Command(BaseCommand):

    help = 'Create random authors'

    def add_arguments(self, parser):
        parser.add_argument(
            '--author',
            default=20,
        )

    def handle(self, *args, **options):
        for _ in range(options['author']):
            AuthorFactory.create()
