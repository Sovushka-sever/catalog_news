# Catalog_News (Каталог новостей)

Тестовое задание

На данный момент находится в разработке

Инфраструктура

- Проект работает с БД SQLite3.

Основной стек: Python 3, Django 3.1

### Технические требования

 - Все необходимые для работы пакеты перечислены в 
requirements.txt
### Запуск приложения
1. Склонируйте себе проект 
   - $ git clone https://gitlab.com/Sovushka-sever/catalog_news.git
2. Установите виртуальное окружение и активируйте его
3. Установите все зависимости 
   - pip install -r requirements.txt
4. После того как все зависимости установятся и завершат свою инициализацию, примените все необходимые миграции:
   - python manage.py makemigrations
   - python manage.py migrate
5. Для доступа к панели администратора создайте администратора:
   - python manage.py createsuperuser
6. Запустите приложение:
   - python manage.py runserver
## Дополнительные команды

Для заполнения базы произвольными авторами воспользуйтесь командой
 - python manage.py random_authors

Для заполнения базы новостями с https://rb.ru воспользуйтесь командой
 - python manage.py parser_news

## Авторы
* **Sovushka-sever** 
